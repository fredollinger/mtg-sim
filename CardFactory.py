#!/usr/bin/env python3

import json
import pandas as pd
from LandCard import LandCard
from CreatureCard import CreatureCard
from Card import Card

class CardFactory:
    def __init__(self):
        pass

        with open('lands.json') as f:
            self.lands = json.load(f)

        with open('creatures.json') as f:
            self.creatures = json.load(f)

    def addCard(self, name):
        pass

    def isLand(self, name):
        return "Plains" == name