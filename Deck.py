#!/usr/bin/env python3

import json
import pandas as pd
from LandCard import LandCard
from CreatureCard import CreatureCard
from Card import Card
from CardFactory import CardFactory

class Deck:
    def __init__(self, fn):
        self.cards = []
        self.rawCards = pd.read_csv(fn)

        for i in self.rawCards.index: 
            for x in range(0, int(self.rawCards['Quantity'][i])):
                self.addCard(self.rawCards['Simple Name'][i])

    # TODO USE CARD FACTORY TO MAKE CARDS
    def addCard(self, name):
        if self.isLand(name):
            self.cards.append(LandCard(name))
        else:
            self.cards.append(CreatureCard(name))

    def print(self):
        for card in self.cards:
            print(card.name)

    def isLand(self, name):
        return "Plains" == name

deck = Deck("brightspear.csv")
deck.print()